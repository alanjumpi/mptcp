#!/usr/bin/python

import socket
import mptcp_api #MPTCP API python interface

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
	connid = mptcp_api.get_connid(s)	
finally:
	s.close()

print connid 
