
import netifaces
import socket

MPTCP_ALLOWED_INTERFACES = 500 # ID of the socket option from the kernel

def set_allowed_interfaces(s, intfs):
  if not intfs:
    raise Exception('Need at least one interface!')

  # bind the socket to one of the interfaces
  # the primary mptcp flow will be initiated from there
  set_primary = False
  for intf in intfs:
    try:
      info = netifaces.ifaddresses(intf)
      ipv4 = info[netifaces.AF_INET]
      addr = ipv4[0]['addr'] # pick the first address (any is fine)
      s.bind((addr, 0))
      set_primary = True
      break
    except:
      pass
  if not set_primary:
    raise Exception('Could not bind socket to any of the given interfaces!')

  intf_str = ','.join(intfs)
  s.setsockopt(socket.IPPROTO_TCP, MPTCP_ALLOWED_INTERFACES, intf_str)

