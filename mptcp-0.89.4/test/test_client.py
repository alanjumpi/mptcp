#!/usr/bin/python

import socket

import mptcpintfs # IMPORT LIBRARY FOR MPTCP INTERFACE CONTROL

SERVER_ADDR = "84.211.66.157"
SERVER_PORT = 40000

ALLOWED_INTERFACES = ['eth0'] # SPECIFY ALLOWED INTERFACES

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
  mptcpintfs.set_allowed_interfaces(s, ALLOWED_INTERFACES) # HERE WE SET THE ALLOWED INTERFACES
  s.connect((SERVER_ADDR, SERVER_PORT))
  while True:
    recv_now = s.recv(1024)
    if not recv_now:
      break
finally:
  s.close()

